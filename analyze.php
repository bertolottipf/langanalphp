<?php
include_once('./class/LettersWordsAnalyze.php');



$n_letters = $_POST['n_letters'];
$d_email = $_POST['d_email'];
$case_insensitive = isset($_POST['case_insensitive']) ? true : false;
$accented_letters_like_not = isset($_POST['accented_letters_like_not']) ? true : false;
$lang = $_POST['lang'];

$analyzer = new LettersWordsAnalyze(
        $lang, 
        $n_letters, 
        array('case_insensitive' => $case_insensitive, 'accented_letters_like_not' => $accented_letters_like_not)
);

//echo $analyzer;
