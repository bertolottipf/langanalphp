<?php

class Errors
{
    const FILE_NOT_FOUND        = 100;
    const FILE_NOT_READABLE     = 101;
}
