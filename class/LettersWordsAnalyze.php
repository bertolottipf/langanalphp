<?php
include_once('Txt.php');
include_once('Errors.php');

class LettersWordsAnalyze
{
	private $lang;
	private $n_letters;
	private $analysis = array();
	private $opt;


	function __construct($lang = "it", $n_letters = 2, $opt = array('case_insensitive' => true, 'accented_letters_like_not' => true))
	{
		$this->lang = "./filedb/" . $lang . ".wl";
		$this->n_letters = $n_letters;
		$this->opt = $opt;

		# riempio $analysis con gli array che conteranno le analisi (con posizione o senza posizione) e altri dati
		$this->analysis["without_position"] = array();
		$this->analysis["with_position"] = array();
		$this->analysis["with_position"]["starting_with"] = array();
		$this->analysis["with_position"]["containing"] = array();
		$this->analysis["with_position"]["ending_with"] = array();
		//$analysis["other_data"] = array();
		//var_dump($this->analysis);

		$this->get_words();

		print(json_encode($this->analysis, JSON_PRETTY_PRINT));
	}

	/**
	 * Ritorna un array di parole prese dal file filedb/{$this->lang}.wl
	 */
	private function get_words()
	{
		try {
			$word;
			$fileName = $this->lang;
      
 			

			if (!file_exists($fileName)) {
				throw new Exception("ERROR " . Errors::FILE_NOT_FOUND . ": FILE NOT FOUND<br>");
			}

			$fp = fopen($fileName, "rb");
			if (!$fp) {
				throw new Exception("ERROR " . Errors::FILE_NOT_READABLE . ": FILE NOT READABLE<br>");
			}

			while (!feof($fp)) {
				if ($this->opt['case_insensitive']) {
					$word = strtolower(fgets($fp));
				}
				if ($this->opt['accented_letters_like_not']) {
					$unwanted_array = array(
						'Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
						'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U',
						'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c',
						'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
						'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y'
					);
					$word = strtr($word, $unwanted_array);
				}

				$word = strtr($word, array("\n" => '' ));

				# analizzo la parola
				$this->analyze_word($word);
			}
			fclose($fp);

		} catch (Exception $e) {
			print($e->getMessage());
		}

	}

	/**
	 * Analizza la parola passata e immettendo i risultati in $analysis
	 */
	private function analyze_word($word)
	{
		for ($l = 0; $l < strlen($word) - $this->n_letters + 1; $l++) {
			$to_add = true;

			# prendo le lettere da analizzare 
			//echo $word . "-" . $l . "-" . $this->n_letters . "<br>";
			$sel_letters = substr($word, $l, $this->n_letters);
			//echo $sel_letters;

			// var_dump($this->analysis);
			
			# vedo se c'è già in $analysis["with_position"]["starting_with"] un gruppo di lettere
			# uguale a quello analizzato e se c'è incremento di +1 il numero di volte trovato
			if ($l == 0) {
				//var_dump($this->analysis["with_position"]["starting_with"]);
				foreach ($this->analysis["with_position"]["starting_with"] as $letters => $times) {
					# se le lettere analizzate nell'array è uguale a quello selezionato dalla parola incremento di uno
					if ($sel_letters === $letters) {
						//echo "ESISTENTE - $sel_letters - $word<br>";
						//echo $this->analysis["with_position"]["starting_with"][$sel_letters] . "<br>";
						$to_add = false;
						$this->analysis["with_position"]["starting_with"][$sel_letters] += 1;
						$this->analysis["without_position"][$sel_letters] += 1;
						break;
					}
				}
				if ($to_add) {
					//echo "DA INSERIRE - $sel_letters - $word<br>";
					$this->analysis["with_position"]["starting_with"][$sel_letters] = 1;
					$this->analysis["without_position"][$sel_letters] =  1;
				}
			}
			# vedo se c'è già in $this->analysis["with_position"]["ending_with"] un gruppo di lettere
			# uguale a quello analizzato e se c'è incremento di +1 il numero di volte trovato
			elseif ($l == strlen($word) - $this->n_letters) {
				foreach ($this->analysis["with_position"]["ending_with"] as $letters => $times) {
					# se le lettere analizzate nell'array è uguale a quello selezionato dalla parola incremento di uno
					if ($sel_letters === $letters) {
						$to_add = false;
						$this->analysis["with_position"]["ending_with"][$sel_letters] += 1;
						$this->analysis["without_position"][$sel_letters] += 1;
						break;
					}
				}
				if ($to_add) {
					$this->analysis["with_position"]["ending_with"][$sel_letters] = 1;
					$this->analysis["without_position"][$sel_letters] = 1;
				}
			}
			# vedo se c'è già in $this->analysis["with_position"]["containing"] un gruppo di lettere
			# uguale a quello analizzato e se c'è incremento di +1 il numero di volte trovato
			else {
				foreach ($this->analysis["with_position"]["containing"] as $letters => $times) {
					# se le lettere analizzate nell'array è uguale a quello selezionato dalla parola incremento di uno
					if ($sel_letters === $letters) {
						$to_add = false;
						$this->analysis["with_position"]["containing"][$sel_letters] += 1;
						$this->analysis["without_position"][$sel_letters] += 1;
						break;
					}
				}
				if ($to_add) {
					$this->analysis["with_position"]["containing"][$sel_letters] = 1;
					$this->analysis["without_position"][$sel_letters] = 1;
				}
			}

		}
	}

}
