<?php

class Txt
{
	/**
	 * read a $filename file
	 * return a $array_lines array in with all line in an element
	 */
	static function read_as_array($filename)
	{
		$array_lines = file($filename);
		return $array_lines;
	}

}
