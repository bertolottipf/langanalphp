<?php
require ("../vendor/autoload.php");
use GuzzleHttp\Client;
include_once('../class/LettersWordsAnalyze.php');

$n_letters = $_REQUEST['n_letters'];
$d_email = $_REQUEST['d_email'];
$case_insensitive = isset($_REQUEST['case_insensitive']) ? true : false;
$accented_letters_like_not = isset($_REQUEST['accented_letters_like_not']) ? true : false;
$lang = $_REQUEST['lang'];

/*$analyzer = new LettersWordsAnalyze(
        $lang, 
        $n_letters, 
        array('case_insensitive' => $case_insensitive, 'accented_letters_like_not' => $accented_letters_like_not)
);*/


// Create our client object
$client = new GearmanClient();

// Add a server
$client->addServer(); // by default host/port will be "localhost" & 4730

echo "Sending job\n";

// So a task with gearman worker
$data = json_encode([$d_email, $lang, $n_letters, array('case_insensitive' => $case_insensitive, 'accented_letters_like_not' => $accented_letters_like_not)]);
$result = $client->doNormal("analyze", $data);




if ($result) {
  echo "Success: $result\n";
}