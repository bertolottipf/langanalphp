<?php
require ("/projects/langanal/vendor/autoload.php");
use GuzzleHttp\Client;
include_once('/projects/langanal/class/LettersWordsAnalyze.php');

######################################################################################################
# da http://gearman.org/examples/reverse/                                                            #
#  e https://code.tutsplus.com/tutorials/making-things-faster-with-gearman-and-supervisor--cms-29337 #
######################################################################################################

// Create our worker object
$worker = new GearmanWorker();

// Add a server (again, same defaults apply as a worker)
$worker->addServer();

// Inform the server that this worker can process "reverse" function calls
$worker->addFunction("analyze", "do_analysis");

while (1) {
    print "Waiting for job...\n";
    $ret = $worker->work(); // work() will block execution until a job is delivered
    if ($worker->returnCode() != GEARMAN_SUCCESS) {
        break;
    }
}

// A much simple reverse function
function do_analysis(GearmanJob $job) {
    $workload = $job->workload();
    echo "Received job: " . $job->handle() . "\n";
    echo "Workload: $workload\n";
    
    // decode input
    $data = json_decode($content, true);
    
    // analyze...
    $analysis = new LettersWordsAnalyze(
        $data[0], 
        $data[1], 
        array('case_insensitive' => $data[2][0], 'accented_letters_like_not' => $data[2][1])
    );
    
    // ... and return result
    echo "Analysis: $analysis\n";
    return json_encode($analysis);
    
}
