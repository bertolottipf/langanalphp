<?php
require "./vendor/autoload.php";
use GuzzleHttp\Client;

require 'utils/alert.php';
require 'conf.php';
require "template/header.php";


$path = explode("/", $_SERVER['REQUEST_URI']);
//echo $_SERVER['REQUEST_URI'];

$url = "http://" . $_SERVER['HTTP_HOST'] . '/' . $proj_dirname ."/pages/";


$pagina_e_parametri = array();


var_dump($path);

# prendo la pagina richiesta
if (empty($path[2])) {
    # nel caso non sia dichiarato index (né e tanto meno la pagina)
    $url .= 'home';
    $url .= '.php';
    //alert("1");
} elseif ( ($path[2] == 'index' || $path[2] =='index.php') && empty($path[3])) {
    # nel caso sia dichiarato index ma non la pagina
    $url .= 'home';
    $url .= '.php';
    //alert("2");
} elseif (($path[2] == 'index' || $path[2] =='index.php') && !empty($path[3])) {
    //alert("3");
    # nel caso sia dichiarato index e la pagina)
    # prendo la pagina desiderata
    $desired_page = $path[3];
    # dato che ci posso essere dei parametri passati per querystring, splitto!
    $pagina_e_parametri = explode("?", $desired_page);
    $desired_page = $pagina_e_parametri[0];
    # assegno all'url la pagina con parametri se esiste il file richiesto, altrimenti 404
    //alert($desired_page);
    $url .= file_exists('./pages/' . $desired_page . '.php') ? $desired_page : '404';
    $url .= '.php';
    if (isset($pagina_e_parametri[1]))
        $url .= '?' . $pagina_e_parametri[1];
}


# se ci sono parametri li aggiungo
if (isset($pagina_e_parametri) && isset($pagina_e_parametri[1]) && $pagina_e_parametri[1] != "") {
    $url .=  "?" . $pagina_e_parametri[1];
}


$client = new \GuzzleHttp\Client();
$res = $client->request('GET', $url);
//echo $res->getStatusCode();
// 200
//echo $res->getHeaderLine('content-type');
// 'application/json; charset=utf8'
echo $res->getBody();
// '{"id": 1420053, "name": "guzzle", ...}'


//alert($url);


require 'template/footer.php';
?>

