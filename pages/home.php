<main role="main">

	<section class="jumbotron text-center">
		<div class="container">
			<h1 class="jumbotron-heading">Language Analyze</h1>
			<p class="lead">Take your language analyzed for your interest.</p>
		</div>
	</section>
	<div class="container">
		<div class="row">
		
		<?php
		include('../conf.php');
		foreach ($languages as $language) {
		?>
			<div class="col-md-3">
				<div class="card mb-3 shadow-sm">
					<?php echo '<img class="card-img-top" src="http://' . $_SERVER['HTTP_HOST'] . '/' . $proj_dirname . '/images/flags/' . $language . '.png" alt="' . $language . ' [100%x225]" style="height: 126px; width: 100%; display: block;">'; ?>
					<div class="card-body">
						<!-- <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p> -->
						<div class="d-flex justify-content-between align-items-center">
							<div class="btn-group d-inline-block">
								<button type="button" class="btn btn-sm btn-outline-secondary" onclick="window.location.href=''">Example</button><!-- commento per non far staccare i button
								--><button type="button" class="btn btn-sm btn-outline-secondary" onclick="window.location.href='<?php echo '/' . $proj_dirname ?>/index.php/request_analysis?lang=<?php print($language) ?>'">See / Download</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		} //foreach
		?>
		</div>
	</div>

</main>