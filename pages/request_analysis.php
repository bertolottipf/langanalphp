<?php require('../conf.php'); ?>
<?php require('../utils/alert.php'); ?>

<main role="main">
	<div class="container" id="page">
		<h3 class="h3 mb-3 font-weight-bold">Request analysis</h3>

		<form action="<?php echo '/' . $proj_dirname ?>/gearman/analysis_client.php" method="get">
			
			<div class="form-group row">
				<label for="n_letters" class="col-xl-2 col-form-label">N. of letter to analyze</label>
				<div class="col-xl-7">
					<input type="number" name="n_letters" id="n_letters" class="form-control" required>
				</div>
			</div>

			<div class="form-group row">
				<label for="d_email" class="col-xl-2 col-form-label">Your email</label>
				<div class="col-xl-7">
					<input type="email" name="d_email" id="d_email" class="form-control">
				</div>
			</div>

			<fieldset class="form-group">
				<legend>Options</legend>

				<label for="accented_letters_like_not">
					<input type="checkbox" id="accented_letters_like_not" name="accented_letters_like_not" value="1" checked />
					Accented letter like none <em>(ex.: à = a, ç = c)</em>
				</label>
				<br>
				

				<label for="case_insensitive">
					<input type="checkbox" id="case_insensitive" name="case_insensitive" value="1" checked />
					Uppercase and lowercase letters are the same <em>(ex.: A = a, È = è)</em>
				</label>

				<input type="hidden" name="lang" id="lang" value="<?php echo isset($_GET["lang"]) ?  $_GET["lang"] : "it"; ?>">

			</fieldset>

			<button class="btn btn-lg btn-primary btn-block col-sm-3 ml-auto" type="submit">Request</button>
		</form>
		
	</div>
</main>